﻿using System;
using System.Collections;
using System.Collections.Generic;
using ChristmasLib.Asset;
using ChristmasLib.ChristmasUI2.Buttons;
using ChristmasLib.Utils;
using ChristmasLib.Wrappers;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ChristmasLib.ChristmasUI2
{
    public static class ChristmasUIManager
    {

        
        public const string MenuCameraPagePath =
            "CanvasGroup/Container/Window/QMParent/Menu_Camera";

        public const string CameraPageButton =
            "CanvasGroup/Container/Window/Page_Buttons_QM/HorizontalLayoutGroup/Page_Camera";

        public const string ToggleButtonPath =
            "CanvasGroup/Container/Window/QMParent/Menu_QM_GeneralSettings/Panel_QM_ScrollRect/Viewport/VerticalLayoutGroup/YourAvatar/QM_Settings_Panel/VerticalLayoutGroup/AllowAvatarCloning";

        public const string EmojiQmButton =
            "CanvasGroup/Container/Window/QMParent/Menu_Dashboard/ScrollRect/Viewport/VerticalLayoutGroup/Buttons_QuickActions/Button_Emojis";

        public const string UserPagePath =
            "CanvasGroup/Container/Window/QMParent/Menu_SelectedUser_Local/ScrollRect/Viewport/VerticalLayoutGroup/Buttons_UserActions";

        //private const string AssetBundleUrl = "https://i.uguu.se/ZtHWLzdV";
        private const string AssetBundleUrl = "https://files.catbox.moe/2n8dzp.bundle";

        public const string BundlePath = @"Christmas\Resources\ChristmasLib.bundle";


        public static ChristmasUIPage MainPage, UserPage;

        //public static ChristmasUIPage MovementPage;
        public static Dictionary<string, ChristmasUIPage> MenuPages = new Dictionary<string, ChristmasUIPage>();
        public static Dictionary<string, QMButton> MenuButtons = new Dictionary<string, QMButton>();

        public static Sprite Icon, InfoIcon;
        public static MenuStateController MenuState;
        public static GameObject EmojiButton, CameraButton, QmToggleButton, SelectUserButtonParent;

        public static List<Action> OnUiInitActions = new List<Action>();


        public static IEnumerator UICheck()
        {
            //Download asset bundle
            DownloadHandler.DownloadFileSync(AssetBundleUrl, BundlePath);
            //MelonCoroutines.Start(StatusHandler.DownloadStatus());
            //TODO optimize to a patch maybe?
            //Wait for QuickMenu to be instantiated
            while (UiWrappers.GetVrcUiManager() == null) yield return new WaitForSeconds(1);
            while (UiWrappers.GetVrcUiManager().field_Private_GameObject_0 == null) yield return new WaitForSeconds(1);
            while (UiWrappers.GetQuickMenu() == null) yield return new WaitForSeconds(1);
            while (GetMenuState() == null) yield return new WaitForSeconds(1);

            InitUI();
        }


        private static void InitUI()
        {
            AssetHandler.LoadAssetBundle(BundlePath);
            Icon = AssetHandler.LoadSprite(BundlePath, "BabaIcon");
            InfoIcon = AssetHandler.LoadSprite(BundlePath, "Baba");
            CameraButton = GetQMRoot().transform.Find(CameraPageButton).gameObject;
            EmojiButton = GetQMRoot().transform.Find(EmojiQmButton).gameObject;
            QmToggleButton = GetQMRoot().transform.Find(ToggleButtonPath).gameObject;
            SelectUserButtonParent = GetQMRoot().transform.Find(UserPagePath).gameObject;

            var tabButton = new TabButton("ChristmasPageButton", "Christmas Client Menu", "ChristmasPage", Icon,
                CameraButton.transform.parent, CameraButton);
            MainPage = new ChristmasUIPage("ChristmasPage", InfoIcon, "ChristmasGang");

            UserPage = AddPageByName("Christmas User", "Targeted User menu", SelectUserButtonParent.transform);

            foreach (var uiAction in OnUiInitActions)
                try
                {
                    uiAction?.Invoke();
                }
                catch (Exception e)
                {
                    ConsoleUtils.Error("Error invoking UIInitAction: " + uiAction?.Method.Name + " " + e);
                }
        }
        
        public enum ButtonType
        {
            SingleButton,
            ToggleButton
        }
        
        public static Transform GetQMRoot()
        {
            return UiWrappers.GetVrcUiManager().field_Private_GameObject_0.transform;
        }

 

        /*
           /// <summary>
           /// Updates the info panel of every MenuPage with a new status
           /// </summary>
           //could optimize by making it a non static method inside each page and trigger it on page access instead of every page every time the menu is opened

           public static void UpdateStatus()
           {
               foreach (var p in MenuPages)
               {
                   var r = Random.RandomRangeInt(0, StatusHandler.Statuses.Length - 1);
                   //p.Value.ChangePanelInfo(StatusHandler.Statuses[r]);
               }
           }
   */
        /// <summary>
        /// Get ChristmasUIPage from the MenuPages Dictionary.
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Returns the Value or null if it isn't found</returns>
        public static ChristmasUIPage GetPageByName(string key)
        {
            if (MenuPages.ContainsKey(key)) return MenuPages[key];
            ConsoleUtils.Error("Couldn't find page: " + key);
            return null;
        }

        public static ChristmasUIPage AddPageByName(string key, string tooltip = "Christmas",
            Transform buttonParent = null,
            Action qmButtonAction = null)
        {
            var parent = buttonParent;

            if (!MenuPages.ContainsKey(key))
            {
                if (buttonParent == null) parent = MainPage.ButtonTransform;
                
                QMButton button = new QMButton("Christmas" + key + "Button", tooltip, key, Icon,
                    parent, EmojiButton, () =>
                    {
                        SetPage("Christmas" + key + "Page");
                        qmButtonAction?.Invoke();
                    });


                var page = new ChristmasUIPage("Christmas" + key + "Page", InfoIcon, key, button);
                MenuPages.Add(key, page);
                MenuButtons.Add(key, button);
            }

            return MenuPages[key];
        }

        public static void DestroyPageByName(string key)
        {
            var christmasKey = key;

            var page = GetPageByName(christmasKey);
            if (page != null)
            {
                MenuPages.Remove(christmasKey);
                Object.Destroy(page.ThisPage);
            }
            else
            {
                ConsoleUtils.Error("Failed to remove page: " + christmasKey);
            }
        }


        public static MenuStateController GetMenuState()
        {
            if (MenuState == null)
            {
                MenuState = UiWrappers.GetQuickMenu().GetComponent<MenuStateController>();
            }


            return MenuState;
        }

        public static void SetPage(string pageName)
        {
            
            GetMenuState().Method_Public_Void_String_UIContext_Boolean_EnumNPublicSealedvaNoLeRiBoIn6vUnique_0(pageName,
                null, true, UIPage.EnumNPublicSealedvaNoLeRiBoIn6vUnique.Right);

            GetMenuState().Method_Public_Void_String_UIContext_Boolean_Boolean_0(pageName);

        }

        public static void GoBack(UIPage uiPage)
        {
            var menuState = GetMenuState();
            menuState.Method_Public_Void_UIPage_0(uiPage);
        }
    }
}