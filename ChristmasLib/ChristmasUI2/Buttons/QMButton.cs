﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ChristmasLib.ChristmasUI2.Buttons
{
    public class QMButton : BaseButton
    {
        public QMButton(string name, string tooltip, string text, Sprite icon, Transform parent,
            GameObject buttonToClone, Action onClick = null)
        {
            ThisButton = Object.Instantiate(buttonToClone, parent, true);

            ThisButton.name = name;
            ThisButton.SetActive(true);
            SetIcon(icon);
            SetTooltip(tooltip);
            SetOnclick(onClick);
            SetText(text);
        }
    }

}