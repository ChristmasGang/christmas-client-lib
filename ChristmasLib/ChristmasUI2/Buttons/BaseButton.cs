﻿using System;
using ChristmasLib.Asset;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ChristmasLib.ChristmasUI2.Buttons
{
    public class BaseButton
    {
        public GameObject ThisButton;

        public void SetIcon(Sprite icon)
        {
         //   ChristmasUIManager.Icon = AssetHandler.LoadSprite(ChristmasUIManager.BundlePath, "BabaIcon");
         if (icon == null)
         {
             icon = AssetHandler.LoadSprite(ChristmasUIManager.BundlePath, "Baba");
         }

         var iconTransform = ThisButton.transform.FindChild("Icon");
            if (!iconTransform) iconTransform = ThisButton.transform.FindChild("Icon_On");
            iconTransform.GetComponent<Image>().overrideSprite = icon;
        }

        public void SetTooltip(string text)
        {
            if (ThisButton == null) return;
            var uiTooltip = ThisButton.GetComponent<VRC.UI.Elements.Tooltips.UiTooltip>();

            uiTooltip.Method_Public_UiTooltip_String_1(text);
            // uiTooltip.field_Public_String_1 = text;
        }

        public void SetOnclick(Action onClick)
        {
            ThisButton.GetComponent<Button>().onClick.AddListener(onClick);
        }

        public void SetText(string text)
        {
            var textMesh = ThisButton.GetComponentInChildren<TextMeshProUGUI>();
            textMesh.text = text;
        }
    }
}