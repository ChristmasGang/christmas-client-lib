﻿using UnityEngine;

namespace ChristmasLib.ChristmasUI2.Buttons
{
    public class TabButton : BaseButton
    {
        public MenuTab MTab;

        public TabButton(string name, string tooltip, string pageName, Sprite icon, Transform parent,
            GameObject buttonToClone)
        {
            ThisButton = Object.Instantiate(buttonToClone, parent, true);
            ThisButton.name = name;
            Object.Destroy(ThisButton.GetComponent<MonoBehaviour1PublicVoVoVoVoVoVoVoVoVoVo1>());
            MTab = ThisButton.GetComponent<MenuTab>();
            MTab.field_Public_String_0 = pageName;
            
            SetIcon(icon);
            SetTooltip(tooltip);
        }
    }
}