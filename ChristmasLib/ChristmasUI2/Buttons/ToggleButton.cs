﻿using System;
using System.Collections;
using MelonLoader;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace ChristmasLib.ChristmasUI2.Buttons
{
    public class ToggleButton : BaseButton
    {
        public ToggleButton(string name, string tooltip, string text, Sprite icon, Transform parent,
            GameObject buttonToClone, Action<bool> onToggle = null, bool defaultState = false)
        {
            ThisButton = Object.Instantiate(buttonToClone, parent, true);

            ThisButton.name = name;
            //SetIcon(icon);
            SetTooltip(tooltip);
            SetOnToggle(onToggle);
            SetText(text);
            ThisButton.SetActive(true);
            if (defaultState)
            {
                MelonCoroutines.Start(DelayToggleState());
            }
        }
        // TODO find a way to actually display the toggle update when the button isn't shown
        private IEnumerator DelayToggleState()
        {

            yield return new WaitForSeconds(3);

            SetToggleState(true);

        }

        public void SetOnToggle(Action<bool> onToggle)
        {
            GetToggle().onValueChanged.AddListener(onToggle);
        }

        public void SetToggleState(bool toggle)
        {
            GetToggle().isOn = toggle;
            GetToggle().Set(toggle);

        }

        public Toggle GetToggle()
        {
            return ThisButton.GetComponent<Toggle>();
        }
    }

}