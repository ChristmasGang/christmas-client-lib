﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ChristmasLib.ChristmasUI2.Buttons
{
    public class SingleButton : BaseButton
    {
        public SingleButton(string name, string tooltip, string text, Sprite icon, Transform parent,
            GameObject buttonToClone, Action onClick = null)
        {
            ThisButton = Object.Instantiate(buttonToClone, parent, true);

            ThisButton.name = name;
            SetIcon(icon);
            SetTooltip(tooltip);
            SetOnclick(onClick);
            ThisButton.SetActive(true);
            SetText(text);
        }
    }
}