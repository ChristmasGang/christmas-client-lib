﻿using System;
using System.Linq;
using ChristmasLib.ChristmasUI2.Buttons;
using ChristmasLib.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace ChristmasLib.ChristmasUI2
{
    public class ChristmasUIPage
    {
        public GameObject ThisPage;
        public UIPage uiPage;
        public Transform ButtonTransform;
        public QMButton QMButton;
        public string Name;
        public Transform VerticalLayoutGroup;

        public ChristmasUIPage(string name, Sprite infoIcon, string header, QMButton qmButton = null)
        {
            var foundPage = ChristmasUIManager.GetQMRoot().transform.Find(ChristmasUIManager.MenuCameraPagePath)
                .gameObject;
            ThisPage = Object.Instantiate(foundPage, foundPage.transform.parent, true);
            QMButton = qmButton;
            Name = name;
            ThisPage.name = name;
            VerticalLayoutGroup = ThisPage.transform.GetComponentInChildren<VerticalLayoutGroup>().transform;
            Object.Destroy(ThisPage.GetComponent<UIPage>());
            // ChristmasUiPage = ThisPage.AddComponent<UIPage>();
            uiPage = ThisPage.AddComponent<UIPage>();

            uiPage.field_Public_String_0 = name;
            uiPage.field_Protected_MenuStateController_0 = ChristmasUIManager.GetMenuState();
            uiPage.field_Private_List_1_UIPage_0.Add(uiPage);
            uiPage.field_Public_Boolean_1 = true;
            AddToDictionary(name);
            //ChangePanelInfo(StatusHandler.Status, infoIcon);
            ClearHeaders();
          //  DestroyInfo();
            SetHeader(header);
            RemoveButtons();
            ButtonTransform = ThisPage
                .GetComponentInChildren<GridLayoutGroup>(true).transform;

            AddBackButton();
        }

        public void DestroyInfo()
        {
            var go = VerticalLayoutGroup.transform.Find("Buttons (1)");
            Object.Destroy(go.gameObject);
        }

        public void ClearHeaders()
        {
            var header = VerticalLayoutGroup.GetComponentsInChildren<Transform>();
            for (var i = 0; i < header.Count; i++)
            {
                if (header[i].name == "Header_H3")
                {
                    Object.Destroy(header[i].gameObject);
                }
            }
        }

        /*
        private void AddBackButton()
        {
            var header = ThisPage.transform.FindChild("Header_Camera/LeftItemContainer");
            if (header == null) return;
            var backButton = Find("UserInterface/Canvas_QuickMenu(Clone)/Container/Window/QMParent/Menu_UserIconCamera/Header_H1/LeftItemContainer/Button_Back");
            var newBack = Object.Instantiate(backButton, header);

        }
        */


        private int _backButtonCounter;

        private void AddBackButton()
        {
            var header = ThisPage.transform.FindChild("Header_Camera/LeftItemContainer");
            if (header == null) return;
            _backButtonCounter++;
            var singleButton = new SingleButton("Christmas" + "back" + _backButtonCounter + "Button", "Go back", "back",
                ChristmasUIManager.Icon,
                header, ChristmasUIManager.EmojiButton, () => ChristmasUIManager.GoBack(uiPage));
            singleButton.ThisButton.transform.FindChild("Background").GetComponent<Transform>().localScale =
                new Vector3(0.5f, 0.5f, 0.5f);
            var icon = singleButton.ThisButton.transform.FindChild("Icon").GetComponent<Transform>();
            icon.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            icon.localPosition = new Vector3(0f, 0f, 0f);

            singleButton.ThisButton.transform.FindChild("Text_H4").GetComponent<Transform>().localPosition =
                new Vector3(0f, 0f, 0f);
        }


        /// <summary>
        /// Add a button to the parent page
        /// Button Type being SingleButton or ToggleButton
        /// </summary>
        /// <returns>A SingleButton, Toggle Button or empty BaseButton</returns>
        public BaseButton AddButton(ChristmasUIManager.ButtonType type, string name, string tooltip = "Christmas",
            Action onClick = null,
            Action<bool> onToggle = null, bool defaultState = false)
        {
            switch (type)
            {
                case ChristmasUIManager.ButtonType.SingleButton:
                    var button = new SingleButton("Christmas" + name + "Button", tooltip, name, ChristmasUIManager.Icon,
                        ButtonTransform, ChristmasUIManager.EmojiButton, onClick);
                    return button;

                case ChristmasUIManager.ButtonType.ToggleButton:
                    var toggle = new ToggleButton("Christmas" + name + "Button", tooltip, name, ChristmasUIManager.Icon,
                        ButtonTransform, ChristmasUIManager.QmToggleButton, onToggle, defaultState);
                    return toggle;
                default:
                    ConsoleUtils.Error(
                        "Invalid button type enum, please only use Single Button and Toggle Button, got: " + type);
                    var baseButton = new BaseButton();
                    return baseButton;
            } //<-- idiomatic
        }

        public ChristmasUIPage SetHeader(string text)
        {
            //var header = ThisPage.transform.FindChild("Header_Camera").gameObject;
            var header = ThisPage.GetComponentInChildren<LayoutElement>();
            if (header != null)
            {
                var textMesh = header.GetComponentInChildren<TextMeshProUGUI>();
                textMesh.text = text;
            }

            return this;
        }

        public void AddToDictionary(string pageName)
        {
            var menuStateController = ChristmasUIManager.GetMenuState();
            if (uiPage == null)
            {
                Console.WriteLine("Tried to add null page: " + pageName);
            }

            if (menuStateController == null)
            {
                Console.WriteLine("Tried to add null menuStateController: " + menuStateController);
            }

            menuStateController.field_Private_Dictionary_2_String_UIPage_0.Add(pageName, uiPage);

            menuStateController.field_Public_ArrayOf_UIPage_0 =
                menuStateController.field_Public_ArrayOf_UIPage_0.Append(uiPage).ToArray();
        }

        public ChristmasUIPage RemoveButtons()
        {
            Button[] buttons = ThisPage.GetComponentsInChildren<Button>(true);
            foreach (var b in buttons)
            {
                if (b.gameObject.name.StartsWith("Christmas" + "back"))
                {
                    continue;
                }

                Object.Destroy(b.gameObject);
            }

            Toggle[] toggles = ThisPage.GetComponentsInChildren<Toggle>(true);
            foreach (var t in toggles) Object.Destroy(t.gameObject);
            return this;
        }

        public ChristmasUIPage ChangePanelInfo(string text, Sprite sprite = null)
        {
            var panelInfo = ThisPage.transform.FindChild("Panel_Info").gameObject;
            var textMesh = panelInfo.GetComponentInChildren<TextMeshProUGUI>();
            textMesh.text = text;
            if (sprite != null)
            {
                var rawImage = panelInfo.GetComponentInChildren<RawImage>();
                rawImage.m_Texture = sprite.texture;
            }

            return this;
        }

        public ChristmasUIPage SetPanelInfoState(bool state)
        {
            var panelInfo = ThisPage.transform.FindChild("Panel_Info").gameObject;
            panelInfo.SetActive(state);
            return this;
        }
    }
}