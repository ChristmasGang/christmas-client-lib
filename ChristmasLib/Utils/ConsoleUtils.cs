﻿using System;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using ChristmasLib.Internal;

namespace ChristmasLib.Utils
{
    public static class ConsoleUtils
    {
        #region Logging

        private const string ClearColor = "\u001b[0m";
        const string RedColor = "\u001b[38;2;255;0;0m";
        const string GreenColor = "\u001b[38;2;0;255;0m";
        const string BlueColor = "\u001b[38;2;0;0;255m";
        const string YellowColor = "\u001b[38;2;255;255;0m";
        const string CyanColor = "\u001b[38;2;0;255;255m";
        
        
        public static void Write<T>(T input, string mod = "Christmas")
        {
            var time = "[" + DateTime.Now.ToString("HH:mm:ss") + "]";
            var modText = " [" + mod + "] ";
            StringBuilder output = new StringBuilder(RedColor + time + ClearColor+ GreenColor + modText + ClearColor + input);
            Console.WriteLine(output.ToString());
        }
        
        public static void Warning<T>(T input, string mod = "Christmas")
        {
            var time = "[" + DateTime.Now.ToString("HH:mm:ss") + "]";
            var modText = " [" + mod + "] ";
            StringBuilder output = new StringBuilder(RedColor + time + ClearColor + GreenColor + modText + YellowColor + input + ClearColor);
            Console.WriteLine(output.ToString());
        }

        public static void Error<T>(T input, string mod = "Christmas")
        {
            var time = "[" + DateTime.Now.ToString("HH:mm:ss") + "]";
            var modText = " [" + mod + "] ";
            StringBuilder output = new StringBuilder(RedColor + time + modText + input + ClearColor);
            Console.WriteLine(output.ToString());
        }

        public static void Debug<T>(T input, string mod = "Christmas")
        {
            if (!PluginSettings.PluginCfg.Debug) return;
            var time = "[" + DateTime.Now.ToString("HH:mm:ss") + "]";
            var modText = " [" + mod + "] ";
            StringBuilder output = new StringBuilder(RedColor + time + ClearColor + GreenColor + modText + ClearColor + CyanColor + input + ClearColor);
            Console.WriteLine(output.ToString());
        }
        
        //adapted from https://stackoverflow.com/a/60492990
        //input a string with [] surrounding colored elements with non surrounding elements being the alt color
        //Rewriting this for ansi is going to be aids
        /*
        public static void WriteColor(string message, ConsoleColor color, ConsoleColor altColor, string mod = "Christmas")
        {
            
            Console.ResetColor();
            string time = "[" + DateTime.Now.ToString("HH:mm:ss") + "]";
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(time);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(" [" + mod + "] ");
            var pieces = Regex.Split(message, @"(\[[^\]]*\])");

            foreach (var t in pieces)
            {
                string piece = t;
        
                if (piece.StartsWith("[") && piece.EndsWith("]"))
                {
                    Console.ForegroundColor = color;
                    piece = piece.Substring(1,piece.Length-2);          
                }
                else
                {
                    Console.ForegroundColor = altColor;
                }

                Console.Write(piece);
                Console.ResetColor();
            }
    
            Console.WriteLine();
        }
        */
        
        public static void WriteColor(string input, Color color, Color altColor, string mod = "Christmas")
        {
            
            var time = "[" + DateTime.Now.ToString("HH:mm:ss") + "]";
            var modText = " [" + mod + "] ";
            StringBuilder output = new StringBuilder(RedColor + time + ClearColor + GreenColor + modText + ClearColor);
            
            var pieces = Regex.Split(input, @"(\[[^\]]*\])");
            var colorCode1 =  "\u001B[38;2;" + color.R + ";" + color.G + ";" + color.B + "m";
            var colorCode2 =  "\u001B[38;2;" + altColor.R + ";" + altColor.G + ";" + altColor.B + "m";

            
            foreach (var t in pieces)
            {
                var piece = t;
        
                if (piece.StartsWith("[") && piece.EndsWith("]"))
                {
                    output.Append(colorCode1);
                    piece = piece.Substring(1,piece.Length-2);     
                    Debug(piece);

                }
                else
                {
                    output.Append(colorCode2);
                    Debug(piece);

                }
                output.Append(piece);

                //output.Append(piece);
                output.Append(ClearColor);

            }
            Console.WriteLine(output.ToString());

        }
        
        #endregion
        
        public static void Clear()
        {
            Console.Clear();    
        }
        
    }
}
