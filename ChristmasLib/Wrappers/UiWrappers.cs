﻿using ChristmasLib.Extensions;
using System;
using System.Linq;
using UnityEngine;
using VRC;
using VRC.UI;
using VRC.UI.Core;

namespace ChristmasLib.Wrappers
{
   public static class UiWrappers
    {

        #region Quick Menu  
        public static QuickMenu GetQuickMenu() { return Resources.FindObjectsOfTypeAll<QuickMenu>().First(); }

      //  public static UserInteractMenu GetUserInteractMenu() { return Resources.FindObjectsOfTypeAll<UserInteractMenu>()[0]; }

        public static VRCUIManager GetVrcUiManager() { return VRCUIManager.prop_VRCUIManager_0; }
        #endregion

        #region ESP

        public static HighlightsFX GetHighlightsFX()
        {
            return HighlightsFX.prop_HighlightsFX_0;
            
        }

        public static void EnableOutline(this HighlightsFX instance, Renderer renderer, bool state) => instance.Method_Public_Void_Renderer_Boolean_0(renderer, state); //First method to take renderer, bool parameters
        #endregion

        #region Popup

        public static VRCUiPopupManager GetVrcUiPopupManager()
        {
            return VRCUiPopupManager.prop_VRCUiPopupManager_0;
            
        }

       // public static void AlertPopup(this VRCUiPopupManager manager, string title, string text) => manager.Method_Public_Void_String_String_Single_0(title, text, 10f);

        public static void AlertV2(string title, string content, string buttonName, Action action, string button2, Action action2) => VRCUiPopupManager.prop_VRCUiPopupManager_0.Method_Public_Void_String_String_String_Action_String_Action_Action_1_MonoBehaviour1PublicTeImTeBuSiAcSiCoBoSiUnique_0(title, content, buttonName, action, button2, action2);

        #endregion

        #region NewUI

        public static VRCUIManager GetVrcUiPageManager() { return VRCUIManager.prop_VRCUIManager_0; }

        public static UIManager GetUIManagerImpl() { return UIManager.prop_UIManager_0; }

        #endregion
        #region Select

        public static GameObject GetSelectedPlayerUSpeakerGameObject()
        {
            var player = GetSelectedPlayer();
            var c = player._USpeaker;
            var o = c.gameObject;
            return o;
        }

        public static GameObject GetSelectedPlayerGameObject()
        {
            var player = GetSelectedPlayer();
            var o = player.gameObject;
            return o;
        }

   
      
        public static Player GetSelectedPlayer()
        {
            return  PlayerManager.prop_PlayerManager_0
                .GetPlayerByName(SelectedUserManager.prop_SelectedUserManager_0.field_Private_APIUser_0.displayName);
        }
        /*
        public static VRCPlayer GetSelectedVrcPlayer()
        {

            return GetSelectedPlayer().player
        }
        */
        #endregion

    }
}
