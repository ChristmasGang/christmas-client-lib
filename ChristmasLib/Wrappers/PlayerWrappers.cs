﻿using UnityEngine;
using VRC;
using VRC.SDKBase;

namespace ChristmasLib.Wrappers
{
    public static class PlayerWrappers
    {

        public static Player GetCurrentPlayer()
        {
            return Player.prop_Player_0;
        }
        public static PlayerManager GetPlayerManager()
        {
            return PlayerManager.prop_PlayerManager_0;
        }

        /*
        public static bool IsInVr()
        {
            return !VRCTrackingManager.IsInVR();
        }
*/
        public static VRCPlayerApi GetLocalPlayerApi()
        {
            return Networking.LocalPlayer;
        }

        public static int GetLocalPlayerId()
        {
            return GetLocalPlayerApi().playerId;
        }

        public static GameObject GetPlayerCamera()
        {
            return GameObject.Find("Camera (eye)");
        }
    }
}